LICENSE
The template is created by Inovatik and is offered for free use under the open source MIT license for any type and any number of projects.


FILE STRUCTURE
- index.html holds the entire content
- css/styles.css custom css styling
- js/scripts.js custom js code
- images folder contains all the images
- The rest are files specific to different frameworks and dependencies


FRAMEWORKS & DEPENDENCIES
- Bootstrap https://getbootstrap.com/
- jQuery https://jquery.com/ 
- jQuery Easing https://jqueryui.com/easing/
- Font Awesome for icons https://fontawesome.com/